/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////
#pragma once

const double CAT_POWER_IN_A_JOULE = 0; // Cat's do no work
const char   CAT_POWER            = 'c';

extern double fromCatPowerToJoule(double catPower);
extern double fromJouleToCatPower(double joule);

