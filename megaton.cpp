/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.cpp
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////

#include "megaton.h"

double fromMegatonsToJoule(double megatons)
{
   return megatons / MEGATON_IN_A_JOULE;
}

double fromJouleToMegaton(double joule) 
{
   return joule * MEGATON_IN_A_JOULE;  
}
