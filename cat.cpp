/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPowerToJoule(double catPower) 
{
   return CAT_POWER_IN_A_JOULE * catPower;  // Cats do no work
}

double fromJouleToCatPower(double joule)
{
   return CAT_POWER_IN_A_JOULE * joule;  // Cats still don't do work
}
