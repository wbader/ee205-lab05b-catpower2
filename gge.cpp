/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasolineGallonEquivalentToJoule( double gasolineGallonEquivalent )
{
   return gasolineGallonEquivalent / GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE; 
}

double fromJouleToGasolineGallonEquivalent(double joule)
{
   return joule * GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE; 
}
