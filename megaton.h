/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.h
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////
#pragma once

const double MEGATON_IN_A_JOULE = (1 / 4.184) * 1e-15;
const char   MEGATON            = 'm';

extern double fromMegatonsToJoule(double megatons);
extern double fromJouleToMegaton(double joule);
