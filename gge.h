/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////
#pragma once

const double GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE = (1 / 1.213) * 1e-8;
const char   GASOLINE_GALLON_EQUIVALENT            = 'g';

extern double fromGasolineGallonEquivalentToJoule(double gasolineGallonEquivalent);
extern double fromJouleToGasolineGallonEquivalent(double joule);
