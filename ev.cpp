/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.cpp
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////

#include "ev.h"

double fromJouleToElectronVolts( double joule ) 
{
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
} 

double fromElectronVoltsToJoule( double electronVolts )
{
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}
