#################################################################
###		University Of Hawaii, College of Engingeering
### @brief   Lab 05b - catPower  - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author  Waylon Bader <wbader@hawii.edu>
### @date    17 Feb 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
#################################################################

CC     = g++
CFLAGS = -g -Wall -Wextra

TARGET = catPower

all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp
	
foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp
	
catPower.o: catPower.cpp ev.h megaton.h gge.h foe.h cat.h
	$(CC) $(CFLAGS) -c catPower.cpp
	
catPower: catPower.o ev.o megaton.o gge.o foe.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o megaton.o gge.o foe.o cat.o
	
clean:
	rm -f $(TARGET) *.o
	
test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 e j | grep -q "3.14 e is 5.03083E-19 j"
	@./catPower 3.14 e e | grep -q "3.14 e is 3.14 e"
	@./catPower 3.14 e m | grep -q "3.14 e is 1.2024E-34 m"
	@./catPower 3.14 e g | grep -q "3.14 e is 4.14743E-27 g"
	@./catPower 3.14 e f | grep -q "3.14 e is 5.03083E-43 f"
	@./catPower 3.14 e c | grep -q "3.14 e is 0 c"
	@./catPower 3.14 e x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 m j | grep -q "3.14 m is 1.31378E+16 j"
	@./catPower 3.14 m e | grep -q "3.14 m is 8.19995E+34 e"
	@./catPower 3.14 m m | grep -q "3.14 m is 3.14 m"
	@./catPower 3.14 m g | grep -q "3.14 m is 1.08308E+08 g"
	@./catPower 3.14 m f | grep -q "3.14 m is 1.31378E-08 f"
	@./catPower 3.14 m c | grep -q "3.14 m is 0 c"
	@./catPower 3.14 m x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 g j | grep -q "3.14 g is 3.80882E+08 j"
	@./catPower 3.14 g e | grep -q "3.14 g is 2.37728E+27 e"
	@./catPower 3.14 g m | grep -q "3.14 g is 9.1033E-08 m"
	@./catPower 3.14 g g | grep -q "3.14 g is 3.14 g"
	@./catPower 3.14 g f | grep -q "3.14 g is 3.80882E-16 f"
	@./catPower 3.14 g c | grep -q "3.14 g is 0 c"
	@./catPower 3.14 g x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 f j | grep -q "3.14 f is 3.14E+24 j"
	@./catPower 3.14 f e | grep -q "3.14 f is 1.95983E+43 e"
	@./catPower 3.14 f m | grep -q "3.14 f is 7.50478E+08 m"
	@./catPower 3.14 f g | grep -q "3.14 f is 2.58862E+16 g"
	@./catPower 3.14 f f | grep -q "3.14 f is 3.14 f"
	@./catPower 3.14 f c | grep -q "3.14 f is 0 c"
	@./catPower 3.14 f x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 c j | grep -q "3.14 c is 0 j"
	@./catPower 3.14 c e | grep -q "3.14 c is 0 e"
	@./catPower 3.14 c m | grep -q "3.14 c is 0 m"
	@./catPower 3.14 c g | grep -q "3.14 c is 0 g"
	@./catPower 3.14 c f | grep -q "3.14 c is 0 f"
	@./catPower 3.14 c c | grep -q "3.14 c is 0 c"
	@./catPower 3.14 c x | grep -q "Bad Input: To Unit not found!"
	
	@./catPower 3.14 x j | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x e | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x m | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x g | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x f | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x c | grep -q "Bad input: From Unit not found!"
	@./catPower 3.14 x x | grep -q "Bad input: From Unit not found!"
	
	@./catPower | grep -q "Energy converter"
	
	@echo "All tests pass"
	
	
	
	
	
	

