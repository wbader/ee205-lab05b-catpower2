/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.h
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////
#pragma once

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const char   ELECTRON_VOLT             = 'e';

extern double fromJouleToElectronVolts( double joule );
extern double fromElectronVoltsToJoule( double electronVolts );

