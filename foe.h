/////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering 
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Waylon Bader <wbader@hawaii.edu>
/// @date 17 Feb 2022
/////////////////////////////////////////////////////////////////
#pragma once

const double FOE_IN_A_JOULE = 1e-24;
const char   FOE	     = 'f';

extern double fromFoeToJoule (double foe);
extern double fromJouleToFoe (double joule);
